require('dotenv').config()
const express = require('express')
const cookieParser = require('cookie-parser')
const winston = require('winston')
const expressWinston = require('express-winston')
const apiResponses = require('./app/utils/apiResponses')
const responseCodes = require('./app/utils/responseCodes')

const { LOGGER_OUTPUT_LEVEL } = process.env

const app = express()
app.use(expressWinston.logger({
  level: 'verbose',
  format: winston.format.combine(winston.format.timestamp(), winston.format.logstash()),
  defaultMeta: { service: 'node_project_example' },
  transports: [new winston.transports.Console()]
}))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

const logger = winston.createLogger({
  level: LOGGER_OUTPUT_LEVEL,
  format: winston.format.logstash(),
  defaultMeta: { service: 'node_project_example' },
  transports: [new winston.transports.Console()]
})

// Validate the env variables
require('./tools/validateEnv')(logger)
// Load the Express routes
require('./app/routes/index')(app)

// catch 404
app.use(function (req, res, next) {
  return apiResponses.sendResponse(res, {
    error: {
      name: 'URL Not Found',
      message: 'The path requested was not found.',
      statusCode: responseCodes.PAGE_NOT_FOUND
    },
    data: null
  })
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('NODE_ENV') === 'development' ? err : {}

  // Send generic error
  return apiResponses.sendResponse(res, {
    error: {
      statusCode: err.status || responseCodes.INTERNAL_SERVER_ERROR,
      message: 'An error was encountered.'
    },
    data: null
  })
})

module.exports = app

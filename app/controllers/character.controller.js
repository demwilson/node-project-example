const apiResponses = require('../utils/apiResponses')
const responseCodes = require('../utils/responseCodes')
const characterService = require('../services/character.service')
const {
  characterSchemaCreate,
  characterSchemaUpdate,
  characterSchemaDeleteById,
  characterSchemaGetById
} = require('../schemas/characterSchema')
const logger = require('../utils/logger')

const loggerMeta = {
  page: 'character.controller.js',
  function: null
}

const getCharacters = async (req, res) => {
  logger.silly(
    'Begin getting all characters.',
    { ...loggerMeta, function: 'getCharacters' }
  )
  const ret = await characterService.getCharacters(logger)
  apiResponses.sendResponse(res, ret)
}

const getCharacterById = async (req, res) => {
  logger.silly(
    'Begin getting character by ID.',
    { ...loggerMeta, function: 'getCharacterById' }
  )
  const id = parseInt(req.params.id)
  const result = characterSchemaGetById.validate(id)
  if (result.error) {
    return apiResponses.error(res, {
      statusCode: responseCodes.BAD_REQUEST,
      data: result.error
    })
  }
  const ret = await characterService.getCharacterById(id, logger)
  if (ret.error) {
    return apiResponses.error(res, {
      statusCode: responseCodes.BAD_REQUEST,
      data: ret.error
    })
  }
  return apiResponses.sendResponse(res, ret)
}

const createCharacter = async (req, res) => {
  logger.silly(
    'Begin creating character.',
    { ...loggerMeta, function: 'createCharacter' }
  )
  const characterData = req.body
  const result = characterSchemaCreate.validate(characterData)
  if (result.error) {
    logger.error(
      'Failed to create new character.',
      {
        function: 'createCharacter',
        data: characterData,
        error: result.error
      }
    )
    return apiResponses.error(res, {
      statusCode: responseCodes.BAD_REQUEST,
      data: result.error
    })
  }
  const ret = await characterService.createCharacter(characterData, logger)
  return apiResponses.sendResponse(res, ret)
}

const updateCharacterById = async (req, res) => {
  logger.silly(
    'Begin updating character by ID.',
    { ...loggerMeta, function: 'updateCharacterById' }
  )
  const characterData = {
    ...req.body,
    characterId: req.params.id
  }
  const result = characterSchemaUpdate.validate(characterData)
  if (result.error) {
    logger.error(
      'Begin updating character by ID.',
      { ...loggerMeta, function: 'updateCharacterById' }
    )
    return apiResponses.error(res, {
      statusCode: responseCodes.BAD_REQUEST,
      data: result.error
    })
  }
  const ret = await characterService.updateCharacterById(characterData, logger)
  return apiResponses.sendResponse(res, ret)
}

const deleteCharacterById = async (req, res) => {
  logger.silly(
    'Begin deleting character by ID.',
    { ...loggerMeta, function: 'deleteCharacterById' }
  )
  const id = Number(req.params.id)
  const result = characterSchemaDeleteById.validate(id)
  if (result.error) {
    logger.error(
      'Failed to create new character.',
      {
        function: 'deleteCharacterById',
        data: {
          id
        },
        error: result.error
      }
    )
    return apiResponses.error(res, {
      statusCode: responseCodes.BAD_REQUEST,
      data: result.error
    })
  }
  const ret = await characterService.deleteCharacterById(id, logger)
  return apiResponses.sendResponse(res, ret)
}

module.exports = {
  getCharacters,
  getCharacterById,
  createCharacter,
  updateCharacterById,
  deleteCharacterById
}

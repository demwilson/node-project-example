const responseCodes = require('../utils/responseCodes')
const { executeQuery } = require('../database/database')

const loggerMeta = {
  file: 'character.service.js',
  function: null
}

const getCharacters = async (logger) => {
  logger.debug(
    'Getting all characters.',
    { ...loggerMeta, function: 'getCharacters' }
  )
  const query = `
      SELECT *
      FROM character;
  `
  const ret = await executeQuery(query, logger)
  if (ret.error) {
    logger.error(
      'Failed to retrieve all characters.',
      { ...loggerMeta, function: 'getCharacters', error: ret.error }
    )
    return ret
  }
  return {
    error: null,
    data: ret.data.rows
  }
}

const getCharacterById = async (id, logger) => {
  logger.debug(
    'Getting character by ID.',
    { ...loggerMeta, function: 'createCharacter', data: { id } }
  )
  const query = `
      SELECT *
      FROM character
      WHERE character_id = ${id};
  `
  const ret = await executeQuery(query, logger)
  if (ret.error) {
    logger.error(
      'Failed to retrieve get character by ID.',
      { ...loggerMeta, function: 'getCharacterById', error: ret.error }
    )
    return ret
  }
  if (ret.data && ret.data.rows.length === 0) {
    const error = {
      name: 'CHARACTER_ID_NOT_FOUND',
      message: `A character with id '${id}' was not found.`,
      statusCode: responseCodes.BAD_REQUEST
    }
    logger.error(
      'Failed to create get character by ID.',
      { ...loggerMeta, function: 'getCharacterById', error }
    )
    return {
      error,
      data: null
    }
  }
  return {
    error: null,
    data: ret.data.rows[0]
  }
}

const createCharacter = async (data, logger) => {
  logger.debug(
    'Creating new character.',
    { ...loggerMeta, function: 'createCharacter', data }
  )
  const query = `
      INSERT INTO character (name, class, level)
      VALUES ('${data.name}', '${data.class}', ${data.level})
      RETURNING character_id;
  `
  const ret = await executeQuery(query, logger)
  if (ret.error) {
    logger.error(
      'Failed to insert new character.',
      { ...loggerMeta, function: 'createCharacter', error: ret.error }
    )
    return ret.error
  }
  const newCharacterId = ret.data.rows[0].character_id
  return {
    error: null,
    data: {
      id: newCharacterId
    }
  }
}

const updateCharacterById = async (data, logger) => {
  logger.debug(
    'Updating character by ID.',
    { ...loggerMeta, function: 'updateCharacterById' }
  )
  const query = `
      UPDATE character
      SET class = '${data.class}',
          level = ${data.level}
      WHERE character_id = ${data.characterId};
  `
  const ret = await executeQuery(query, logger)
  if (ret.error) {
    logger.error(
      'Failed to update character by ID.',
      { ...loggerMeta, function: 'updateCharacterById', error: ret.error }
    )
    return ret
  }
  return {
    error: null,
    data: null
  }
}

const deleteCharacterById = async (id, logger) => {
  logger.debug(
    'Deleting character by ID.',
    { ...loggerMeta, function: 'deleteCharacterById' }
  )
  const query = `
      DELETE
      FROM character
      WHERE character_id = ${id};
  `
  const ret = await executeQuery(query, logger)
  if (ret.error) {
    logger.error(
      'Failed to delete character by ID.',
      { ...loggerMeta, function: 'deleteCharacterById' }
    )
    return ret
  }
  return {
    error: null,
    data: null
  }
}

module.exports = {
  getCharacters,
  getCharacterById,
  createCharacter,
  updateCharacterById,
  deleteCharacterById
}

const winston = require('winston')

const { LOGGER_OUTPUT_LEVEL } = process.env
const logger = winston.createLogger({
  level: LOGGER_OUTPUT_LEVEL,
  format: winston.format.combine(winston.format.timestamp(), winston.format.logstash()),
  defaultMeta: { service: 'node_project_example' },
  transports: [new winston.transports.Console()]
})
module.exports = logger

const handleSqlError = (sqlRet, logger) => {
  logger.error(JSON.stringify(sqlRet))
  return {
    error: {
      name: 'SQLError',
      message: `An SQL error was encountered. SQL_ERROR_CODE: ${sqlRet.code}`,
      statusCode: 500
    },
    data: undefined
  }
}
exports.handleSqlError = handleSqlError

const { Pool } = require('pg')
const { handleSqlError } = require('../utils/handleSqlError')

const { NODE_ENV, POSTGRES_URL } = process.env
const PRODUCTION = 'production'

const loggerMeta = {
  file: 'database.js',
  function: null
}

let sslValue = false
if (NODE_ENV === PRODUCTION) {
  sslValue = {
    rejectUnauthorized: false
  }
}
const pool = new Pool({
  connectionString: POSTGRES_URL,
  max: 20,
  ssl: sslValue
})

// Ping database to check for common exception errors.
pool.connect((err, client) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has too many connections.')
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused.')
    }
  }
  // If this threw an error
  if (err && err.stack && err.message) {
    console.error(`[database.js] Database Error: ${err.message}`)
  }
  if (client) client.release()
})

// Promisify for Node.js async/await.
const promiseQuery = async (text, params) =>
  pool.query(text, params).catch((error) => error)

const executeQuery = async (query, logger) => {
  logger.debug(
    'Executing query in database.',
    { ...loggerMeta, function: 'executeQuery' }
  )
  const normalized = query.replace(/\s+/g, ' ').trim()
  const ret = await promiseQuery(normalized)
  if (ret.code) {
    logger.error(
      'Failed to query database.',
      { ...loggerMeta, function: 'executeQuery', query }
    )
    return handleSqlError(ret, logger)
  }
  return {
    data: ret
  }
}

module.exports = {
  executeQuery
}

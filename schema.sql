create database node_project_example;
\c node_project_example;

CREATE TABLE character (
    "character_id" SERIAL GENERATED ALWAYS AS IDENTITY,
    "name" VARCHAR (45) NOT NULL,
    "class" VARCHAR(45) NOT NULL,
    "level" INT NOT NULL,
    PRIMARY KEY ("character_id")
);

INSERT INTO character
VALUES
    (default, 'Tony', 'Barbarian', 4),
    (default, 'Alex', 'Wizard', 6),
    (default, 'Meghan', 'Sorcerer', 17),
    (default, 'Michelle', 'Bard', 19);

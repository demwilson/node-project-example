# Example Node.js Project

This is an example project to learn how to do node.js applications.

## Getting Started

1. Install (Docker)[https://www.docker.com/products/docker-desktop/].
2. Install the dependencies.
```bash
npm install
```

## Run the app

1. Start postgres:
```bash
docker compose up
```

2. If this is your first run, create .env file:
```bash
npm run env
```

3. Start the example node project (defaults to `http://localhost:3000/`):
```bash
npm start
```

## Accessing Dockerized Postgres
To access the postgres database via CLI use this command:
```bash
docker exec -tiu postgres node_project_example psql
```

Select the database:
```postgresql
\c node_project_example
```

## Testing
Import the Postman JSON file to Postman for examples to use locally.

## Linter
To run JavaScript Standard Style with automatic fix:
```bash
npm run lint
```
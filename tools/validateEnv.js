const env = require('env-var')

const loggerMeta = {
  file: 'validateEnv.js',
  function: null
}

const validateVariable = (func) => {
  try {
    func()
  } catch (ex) {
    // Trim all the extra text away from the environment variable that is missing
    const varName = ex.message.substr(10).split('"')[0]
    return varName
  }
  return false
}

const validateEnv = (logger) => {
  const requiredEnvVars = [
    () => env.get('PORT').required().asPortNumber(),
    () => env.get('NODE_ENV').required().asString(),
    () => env.get('POSTGRES_URL').required().asString(),
    () => env.get('LOGGER_OUTPUT_LEVEL').required().asString()
  ]
  const errors = requiredEnvVars.reduce((acc, envVar) => {
    const ret = validateVariable(envVar)
    if (ret) {
      acc.push(ret)
    }
    return acc
  }, [])
  if (errors.length > 0) {
    logger.error(
      `Missing ${errors.length} environment variables.`,
      { ...loggerMeta, errors }
    )
    process.exit(1)
  }
}

module.exports = validateEnv

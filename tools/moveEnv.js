const fs = require('fs')

// File destination.txt will be created or overwritten by default.
fs.copyFile('.env.local', '.env', (err) => {
  if (err) throw err
  console.log("Copied '.env.local' to '.env'.")
})
